#include "conveniencefunctions.h"

#include "../GenericPairingLibrary/genericpairing.h"

#include <string>

#include <cryptopp/osrng.h>
#include <cryptopp/rsa.h>
#include <cryptopp/base64.h>
#include <cryptopp/hex.h>



using namespace GenericPairingDaemon;



std::string ConvenienceFunctions::secByteBlockToStdString(const CryptoPP::SecByteBlock& secByteBlock){
    std::string stdString;
    CryptoPP::StringSource(secByteBlock.data(),secByteBlock.SizeInBytes(),true,
        new CryptoPP::StringSink(stdString)
    );
    return stdString;
}

CryptoPP::SecByteBlock ConvenienceFunctions::stdStringToSecByteBlock(const std::string& stdString){
    return CryptoPP::SecByteBlock((const byte*)stdString.data(),stdString.size());
}

std::string ConvenienceFunctions::publicKeyStringToBase64(const std::string& publicKeyString){
    std::string publicKeyBase64;
    CryptoPP::StringSource(publicKeyString,true,
        new CryptoPP::Base64Encoder(
            new CryptoPP::StringSink(publicKeyBase64),false
        )
    );
    return publicKeyBase64;
}

std::string ConvenienceFunctions::publicKeyBase64ToString(const std::string& publicKeyBase64){
    std::string publicKeyString;
    CryptoPP::StringSource(publicKeyBase64,true,
        new CryptoPP::Base64Decoder(
            new CryptoPP::StringSink(publicKeyString)
        )
    );
    return publicKeyString;
}

std::string ConvenienceFunctions::publicKeyStringToDER(const std::string &publicKeyString){
    CryptoPP::RSA::PublicKey publicKey;
    CryptoPP::StringSource source(publicKeyString,true);
    publicKey.Load(source);

    std::string publicKeyDEREncoded;
    CryptoPP::StringSink ss(publicKeyDEREncoded);
    publicKey.DEREncode(ss);

    return publicKeyDEREncoded;
}

std::string ConvenienceFunctions::publicKeyDERToString(const std::string& publicKeyDER){
    CryptoPP::RSA::PublicKey publicKey;
    CryptoPP::StringSource source(publicKeyDER,true);
    publicKey.BERDecode(source);

    std::string publicKeyAsString;
    CryptoPP::StringSink ss(publicKeyAsString);
    publicKey.Save(ss);

    return publicKeyAsString;
}

std::string ConvenienceFunctions::generateRandom(size_t lengthInBytes){
    CryptoPP::DefaultAutoSeededRNG rng;
    CryptoPP::SecByteBlock sharedSecret(lengthInBytes);
    rng.GenerateBlock(sharedSecret.data(),lengthInBytes);

    return secByteBlockToStdString(sharedSecret);
}

std::string ConvenienceFunctions::getSHA256Hash(const std::string &data){
    CryptoPP::SHA256 hashFunction;

    std::string result;
    CryptoPP::StringSource(data,true,
        new CryptoPP::HashFilter(hashFunction,
            new CryptoPP::StringSink(result)
        )
    );

    return result;
}

std::string ConvenienceFunctions::createSAS(const std::string &data){
    std::string hash = getSHA256Hash(data);
    std::string hex;

    CryptoPP::StringSource ss( hash, true /* PumpAll */,
        new CryptoPP::HexEncoder(
            new CryptoPP::StringSink( hex ),
            true /*UCase*/
        ) // HexEncoder
    ); // StringSource

    return hex.substr(1,GenericPairing::DefaultSasByteLength * 2); // Multiplied by 2 because it's already hex encoded
}

std::string ConvenienceFunctions::encryptWithRSAPublicKey(const std::string& unencrypted, const std::string& publicKeyDER){
    CryptoPP::RSA::PublicKey publicKey;
    CryptoPP::StringSource source(publicKeyDER,true);
    publicKey.BERDecode(source);

    CryptoPP::RSAES<CryptoPP::OAEP<CryptoPP::SHA256>>::Encryptor e(publicKey);
    CryptoPP::DefaultAutoSeededRNG rng;
    std::string encrypted;
    CryptoPP::StringSource(unencrypted, true,
        new CryptoPP::PK_EncryptorFilter( rng, e,
            new CryptoPP::StringSink(encrypted)
        )
    );

    return encrypted;
}
