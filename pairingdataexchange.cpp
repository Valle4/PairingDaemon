#include "pairingdataexchange.h"

#include "conveniencefunctions.h"
#include "sharedsecretgeneration.h"

#include "../GenericPairingLibrary/genericpairing.h"

#include <string>

#include <QTimer>
#include <QDataStream>
#include <QByteArray>
#include <QtDebug>



using namespace GenericPairingDaemon;



bool PairingDataExchange::readMessage_ClientHash(QTcpSocket* pairingPartnerCommunicationSocket, QByteArray* clientHash, QString* error){
    QDataStream tcpStream(pairingPartnerCommunicationSocket);
    GenericPairing::message_t message_raw;
    tcpStream >> message_raw >> *clientHash;
    pairingPartnerCommunicationSocket->readAll(); // Get rid of the trailing \n
    Message message = (Message) message_raw;

    if (message != Message::Pairing_ClientHash){
        *error = "Expected Pairing_ClientHash but got different message";
        return false;
    }

    return true;
}



bool PairingDataExchange::readMessage_ServerPublicKey(QTcpSocket* pairingPartnerCommunicationSocket, QByteArray* serverPublicKey, QString* error){
    QDataStream tcpStream(pairingPartnerCommunicationSocket);
    GenericPairing::message_t message_raw;
    tcpStream >> message_raw >> *serverPublicKey;
    pairingPartnerCommunicationSocket->readAll(); // Get rid of the trailing \n
    Message message = (Message) message_raw;

    if (message != Message::Pairing_ServerPublicKey){
        *error = "Expected Pairing_ServerPublicKey but got different message";
        return false;
    }

    return true;
}



bool PairingDataExchange::readMessage_ClientPublicKey(QTcpSocket* pairingPartnerCommunicationSocket, QByteArray* clientPublicKey, QString* error){
    QDataStream tcpStream(pairingPartnerCommunicationSocket);
    GenericPairing::message_t message_raw;
    tcpStream >> message_raw >> *clientPublicKey;
    pairingPartnerCommunicationSocket->readAll(); // Get rid of the trailing \n
    Message message = (Message) message_raw;

    if (message != Message::Pairing_ClientPublicKey){
        *error = "Expected Pairing_ClientPublicKey but got different message";
        return false;
    }

    return true;
}





void PairingDataExchange::sendMessage_ClientHash(QTcpSocket* pairingPartnerCommunicationSocket, const QByteArray& clientHash){
    QDataStream tcpStream(pairingPartnerCommunicationSocket);
    tcpStream << (GenericPairing::message_t) Message::Pairing_ClientHash
              << clientHash
              << "\n";
    while (pairingPartnerCommunicationSocket->bytesToWrite() > 0)
        pairingPartnerCommunicationSocket->waitForBytesWritten();
}



void PairingDataExchange::sendMessage_ServerPublicKey(QTcpSocket* pairingPartnerCommunicationSocket, const QByteArray& serverPublicKey){
    QDataStream tcpStream(pairingPartnerCommunicationSocket);
    tcpStream << (GenericPairing::message_t) Message::Pairing_ServerPublicKey
              << serverPublicKey
              << "\n";
    while (pairingPartnerCommunicationSocket->bytesToWrite() > 0)
        pairingPartnerCommunicationSocket->waitForBytesWritten();
}



void PairingDataExchange::sendMessage_ClientPublicKey(QTcpSocket* pairingPartnerCommunicationSocket, const QByteArray& clientPublicKey){
    QDataStream tcpStream(pairingPartnerCommunicationSocket);
    tcpStream << (GenericPairing::message_t) Message::Pairing_ClientPublicKey
              << clientPublicKey
              << "\n";
    while (pairingPartnerCommunicationSocket->bytesToWrite() > 0)
        pairingPartnerCommunicationSocket->waitForBytesWritten();
}
