#include "pairingd_global.h"

#include "statickeycontainer.h"
#include "pairingstore.h"
#include "ipcserver.h"
#include "sharedsecretgeneration.h"

#include "../GenericPairingLibrary/genericpairing.h"

#include <QCoreApplication>
#include <QtNetwork>

#include <string>

#define PRIVATEKEY_FILENAME "pairingd.privatekey"
#define PRIVATEKEY_SIZE 4096



using GenericPairingDaemon::IpcServer;
using GenericPairingDaemon::PairingStore;
using GenericPairingDaemon::StaticKeyContainer;



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);



    StaticKeyContainer staticKeyContainer;
    qDebug() << "Attempting to load private key from file " PRIVATEKEY_FILENAME;
    if (!staticKeyContainer.loadPrivateKeyFromFile(PRIVATEKEY_FILENAME) != 0){
        qDebug() << "No existing private key could be loaded, will generate one...";
        staticKeyContainer.generatePrivateKey(PRIVATEKEY_SIZE);
        qDebug() << "Writing the key to " PRIVATEKEY_FILENAME "\n";
        staticKeyContainer.writePrivateKeyToFile(PRIVATEKEY_FILENAME);
    } else
        qDebug() << "Private key loaded successfully\n";


    PairingStore pairingStore(GenericPairing::DefaultPairingPath);
    IpcServer ipcServer(&staticKeyContainer,&pairingStore);



#ifdef Q_OS_ANDROID // on android we have no permissions for unix domain sockets...
    QTcpServer s;
    auto handleConnection = [&s,&ipcServer] () {
        QTcpSocket* localSocket = s.nextPendingConnection();
        QObject::connect(localSocket, &QTcpSocket::readyRead, &ipcServer, &IpcServer::readIpcMessage);
        QObject::connect(localSocket, &QTcpSocket::disconnected, localSocket, &QTcpSocket::deleteLater);
    };
    QObject::connect(&s, &QTcpServer::newConnection, handleConnection);

    if (!s.listen(QHostAddress::Any, GenericPairing::DefaultLoopbackIpcPort)){
        qDebug() << a.tr("IPC server could not listen: ") << s.errorString();
        exit(1);
    }
#else
    QLocalServer s;
    QLocalServer::removeServer(QString::fromStdString(GenericPairing::IpcServerName)); // file might not have been deleted after a previous run
    auto handleConnection = [&s,&ipcServer] () {
        QLocalSocket* localSocket = s.nextPendingConnection();
        QObject::connect(localSocket, &QLocalSocket::readyRead, &ipcServer, &IpcServer::readIpcMessage);
        QObject::connect(localSocket, &QLocalSocket::disconnected, localSocket, &QLocalSocket::deleteLater);
    };
    QObject::connect(&s, &QLocalServer::newConnection, handleConnection);

    if (!s.listen(QString::fromStdString(GenericPairing::IpcServerName))){
        qDebug() << a.tr("IPC server could not listen: ") << s.errorString();
        exit(1);
    }
#endif // Q_OS_ANDROID

    return a.exec();
}
